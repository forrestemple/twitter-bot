<?php


class tweetbot {

    /**
     * @param $tweet
     * @return API|mixed
     */
    function TweetIt($tweet)
    {
        $con = $this->OAuth();
        $status = $con->post('statuses/update', array('status' => $tweet));

        if ($con->lastStatusCode() !== 200)
        {
            echo 'Shit went wrong....';
        } else
        {
            return $status;
        }
    }

    /**
     * @return TwitterOAuth
     */
    function OAuth()
    {
        require_once('../inc/twitteroauth/twitteroauth.php');
        $con = New TwitterOAuth($this->apiKey, $this->apiSeceret, $this->apiAccessToken, $this->apiTokenSeceretKey);

        return $con;
    }

    /**
     * @param $ApiKey
     * @param $ApiSeceret
     * @param $ApiAccessToken
     * @param $ApiTokenSeceretKey
     */
    function SetApiKey($ApiKey, $ApiSeceret, $ApiAccessToken, $ApiTokenSeceretKey)
    {
        $this->apiKey = $ApiKey;
        $this->apiSeceret = $ApiSeceret;
        $this->apiAccessToken = $ApiAccessToken;
        $this->apiTokenSeceretKey = $ApiTokenSeceretKey;
    }
}