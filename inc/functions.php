<?php

require('../inc/twitteroauth/twitteroauth.php');
require('../inc/config.php');
require('../inc/tweetbot.php');

class functions {

    /**
     * @param $pdo
     */
    function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return mixed
     */
    function getTweet()
    {
        $query = $this->pdo->prepare('select * from tweets where tweeted = 0 order by rand() limit 1');
        $query->execute();
        return $query->fetch();
    }

    /**
     * @param $tweet
     * @return mixed
     */
    function insertTweet($tweet)
    {
        $query = $this->pdo->prepare('insert into tweets(tweet,timetweeted) values ("'.$tweet.'",NULL)');
        $query->execute();
        return $query;
    }

    /**
     * @return mixed
     */
    function getAllTweet()
    {
        $query = $this->pdo->prepare('select * from tweets');
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @return mixed
     */
    function countTweets()
    {
        $query = $this->pdo->prepare('select count(*) from tweets');
        $query->execute();
        $rows = $query->fetchColumn();
        return $rows;
    }

    /**
     * @return mixed
     */
    function countTweetedTweets()
    {
        $query = $this->pdo->prepare('select count(*) from tweets where tweeted = 1');
        $query->execute();
        $rows = $query->fetchColumn();
        return $rows;
    }

    /**
     * @return mixed
     */
    function countUnTweetedTweets()
    {
        $query = $this->pdo->prepare('select count(*) from tweets where tweeted = 0');
        $query->execute();
        $rows = $query->fetchColumn();
        return $rows;
    }

    /**
     * @param $id
     */
    function markTweeted($id)
    {
        $query = $this->pdo->prepare('update tweets set tweeted = 1, timetweeted = current_timestamp where id =' . $id);
        $query->execute();
    }

    /**
     * @param $id
     */
    function deleteTweet($id)
    {
        $query = $this->pdo->prepare('delete from tweets where id =' . $id);
        $query->execute();
    }

    /**
     * @param $id
     */
    function resetTweets($id)
    {
        $query = $this->pdo->prepare('update tweets set tweeted = 0, timetweeted = null where id='. $id);
        $query->execute();
    }

    
}