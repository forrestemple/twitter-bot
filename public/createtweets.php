<?php
/**
 * Created by PhpStorm.
 * User: forrestemple
 * Date: 8/2/15
 * Time: 8:44 PM
 */

require('../inc/functions.php');
$tweet = new functions($pdo);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>TMHLive Automagical Twitter Bot</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css">
    <style type="text/css">
        .chars-length {
            background-color: whitesmoke;
            display: inline-block;
            margin: 20px 0 0;
            padding: 10px 30px;
            text-align: center;
            width: 90px
        }

        .warn {
            background-color: firebrick;
            color: #fff;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>

<body>
<p></p>
<div class="container center">
    <div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Total <i class="fa fa-twitter"></i> Tweets</h3>
            </div>
            <div class="panel-body text-center">
                <h3><?php echo $tweet->countTweets(); ?></h3>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-success text-center">
            <div class="panel-heading">
                <h3 class="panel-title">Tweeted <i class="fa fa-twitter"></i> Tweets</h3>
            </div>
            <div class="panel-body">
                <h3><?php echo $tweet->countTweetedTweets() ?></h3>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-warning text-center">
            <div class="panel-heading">
                <h3 class="panel-title">Awaiting <i class="fa fa-twitter"></i> Tweets</h3>
            </div>
            <div class="panel-body text-center">
                <h3><?php echo $tweet->countUnTweetedTweets() ?></h3>
            </div>
        </div>
    </div>

    <div class="jumbotron">
        <h3>Hello, TheMadHatter!</h3>
        <p>Below you will be able to create new tweets, and also see pending and already tweeted tweets.  <i class="fa fa-bug"></i> Bug reports should be sent to jasonk@forrestemple.com via email or on jabber.</p>
        <?php if($_POST)
        {
            $insertTweet = $_POST['tweet'];
            if ($insertTweet != null)
            {
                $tweet->insertTweet($insertTweet);
                echo '<div class="alert alert-info">Congrats you have just submitted a tweet.</div>';
            } else
            {
                echo '<div class="alert alert-danger">Whoops you submitted a empty textform!</div>';}
        }
        ?>
        <form method="post"><textarea class="form-control countdown" id="tweet" name="tweet"></textarea> <i class="fa fa-notice"></i>
        <p><input type="submit" class="btn btn-primary btn-md pull-right" href="#" role="button">Create New Tweet</input></form></p>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Current <i class="fa fa-twitter"></i> Tweets</h3>
        </div>
        <div class="panel-body text-center">
            <table class="table">
                <tr>
                    <td>ID #</td>
                    <td>Tweet</td>
                    <td>Time Tweeted</td>
                </tr>
                <?php foreach($tweet->getAllTweet() as $t): ?>
                <tr <?php if($t['tweeted'] == 1) { echo 'class="danger"'; } else { echo 'class="warning"'; } ?>>
                    <td><?php echo $t['id']; ?></td>
                    <td><?php echo $t['tweet']; ?></td>
                    <td><?php if($t['timetweeted'] == NULL) { echo "Not Yet Tweeted"; } else { echo $t['timetweeted']; } ?></td>
                </tr>
                <?php endforeach ?>
            </table>
        </div>
    </div>
</div>
<script src="/js/vcountdown.min.js" type="text/javascript"></script>
<script type="text/javascript">
    VCountdown.init({
        target: '.countdown'
    });
</script>
</body>

</html>
